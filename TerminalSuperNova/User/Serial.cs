﻿using System;
using System.IO.Ports;
using System.Threading;

namespace TerminalSuperNova.User
{
    public class Serial
    {
        public Serial()
        {
            m_Port = new SerialPort();
            m_Port.ReadTimeout = 1000;
            m_Port.WriteTimeout = 1000;
            ScanPorts();
        }

        public static void ScanPorts()
        {
            AvailablePorts = SerialPort.GetPortNames();
        }

        public static bool ConnectPort(string _portName, int _baudrate, Parity _parity, int _dataBits, StopBits _stopBits)
        {
            DisconnectPort();

            m_Port.PortName = _portName;
            m_Port.BaudRate = _baudrate;
            m_Port.Parity = _parity;
            m_Port.DataBits = _dataBits;
            m_Port.StopBits = _stopBits;

            m_Port.DataReceived += new SerialDataReceivedEventHandler(ReceiveData);

            try
            {
                m_Port.Open();
                HasConnection = true;
                if (SerialPortHasErrors != null)
                    SerialPortHasErrors.Invoke(m_Port.PortName + " is connected");
            }
            catch (Exception ex)
            {
                if (SerialPortHasErrors != null)
                    SerialPortHasErrors.Invoke(ex.Message);
            }
            if (m_Port.IsOpen)
            {
                if (ChangePortStatus != null)
                {
                    ChangePortStatus.Invoke("Disconnect");
                }
                return true;
            }
            else
                return false;
        }

        private static void ReceiveData(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(100);
            if (m_Port.IsOpen)
            {
                int bytesCount = m_Port.BytesToRead;
                byte[] rawData = new byte[bytesCount];
                try
                {
                    m_Port.Read(rawData, 0, bytesCount);
                    if (ReadFromPort != null)
                        ReadFromPort.Invoke(rawData);
                }
                catch (Exception ex)
                {
                    DisconnectPort();
                    if (SerialPortHasErrors != null)
                        SerialPortHasErrors.Invoke(ex.Message);
                }
            }
        }

        public static void WriteToPort(string text)
        {
            if (m_Port.IsOpen)
            {
                try
                {
                    m_Port.Write(text);
                }
                catch (Exception ex)
                {
                    DisconnectPort();
                    if (SerialPortHasErrors != null)
                        SerialPortHasErrors.Invoke(ex.Message);
                }
            }
        }

        public static void DisconnectPort()
        {
            if (m_Port.IsOpen)
            {
                try
                {
                    m_Port.Close();
                }
                catch (Exception ex)
                {
                    m_Port.Dispose();
                    if (SerialPortHasErrors != null)
                        SerialPortHasErrors.Invoke(ex.Message + Environment.NewLine);
                }

                HasConnection = false;
                if (SerialPortHasErrors != null)
                    SerialPortHasErrors.Invoke(m_Port.PortName + " is disconnected");

                if (ChangePortStatus != null)
                {
                    ChangePortStatus.Invoke("Connect");
                }

                m_Port.DataReceived -= new SerialDataReceivedEventHandler(ReceiveData);
            }
        }

        public delegate void SerialPortMessageHandler(string msg);

        public static SerialPortMessageHandler SerialPortHasErrors;
        public static SerialPortMessageHandler ChangePortStatus;

        public delegate void SerialPortReceiveHandler(byte[] data);
        public static SerialPortReceiveHandler ReadFromPort;

        public static string[] AvailablePorts { get; private set; }

        public static bool HasConnection { get; private set; }
        private static SerialPort m_Port { get; set; }
    }
}
