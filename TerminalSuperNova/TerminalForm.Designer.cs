﻿namespace TerminalSuperNova
{
    partial class TerminalSuperNova
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sendButton = new System.Windows.Forms.Button();
            this.portNameSelect = new System.Windows.Forms.ComboBox();
            this.sendText = new System.Windows.Forms.TextBox();
            this.sendBox = new System.Windows.Forms.GroupBox();
            this.clearSendedButton = new System.Windows.Forms.Button();
            this.sendedText = new System.Windows.Forms.RichTextBox();
            this.portSettings = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.stopBitsSelect = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.paritySelect = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataBitsSelect = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.baudrateSelect = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.scanPortsButton = new System.Windows.Forms.Button();
            this.receiveBox = new System.Windows.Forms.GroupBox();
            this.clearReceivedButton = new System.Windows.Forms.Button();
            this.receivedText = new System.Windows.Forms.RichTextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.MustAddSymbols = new System.Windows.Forms.CheckBox();
            this.MustAddCRLF = new System.Windows.Forms.RadioButton();
            this.MustAddCR = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.UseHexadecimalMode = new System.Windows.Forms.RadioButton();
            this.UseDecimalMode = new System.Windows.Forms.RadioButton();
            this.UseTextMode = new System.Windows.Forms.RadioButton();
            this.sendBox.SuspendLayout();
            this.portSettings.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.receiveBox.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(426, 579);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 0;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // portNameSelect
            // 
            this.portNameSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.portNameSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.portNameSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.portNameSelect.FormattingEnabled = true;
            this.portNameSelect.Location = new System.Drawing.Point(6, 19);
            this.portNameSelect.Name = "portNameSelect";
            this.portNameSelect.Size = new System.Drawing.Size(88, 21);
            this.portNameSelect.TabIndex = 1;
            // 
            // sendText
            // 
            this.sendText.Location = new System.Drawing.Point(6, 581);
            this.sendText.Name = "sendText";
            this.sendText.Size = new System.Drawing.Size(414, 20);
            this.sendText.TabIndex = 3;
            this.sendText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.sendButton_Click);
            // 
            // sendBox
            // 
            this.sendBox.Controls.Add(this.clearSendedButton);
            this.sendBox.Controls.Add(this.sendedText);
            this.sendBox.Controls.Add(this.sendText);
            this.sendBox.Controls.Add(this.sendButton);
            this.sendBox.Location = new System.Drawing.Point(12, 111);
            this.sendBox.Name = "sendBox";
            this.sendBox.Size = new System.Drawing.Size(586, 609);
            this.sendBox.TabIndex = 4;
            this.sendBox.TabStop = false;
            this.sendBox.Text = "Send";
            // 
            // clearSendedButton
            // 
            this.clearSendedButton.Location = new System.Drawing.Point(505, 579);
            this.clearSendedButton.Name = "clearSendedButton";
            this.clearSendedButton.Size = new System.Drawing.Size(75, 23);
            this.clearSendedButton.TabIndex = 7;
            this.clearSendedButton.Text = "Clear";
            this.clearSendedButton.UseVisualStyleBackColor = true;
            this.clearSendedButton.Click += new System.EventHandler(this.clearSendedButton_Click);
            // 
            // sendedText
            // 
            this.sendedText.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.sendedText.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendedText.DetectUrls = false;
            this.sendedText.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sendedText.Location = new System.Drawing.Point(6, 19);
            this.sendedText.Name = "sendedText";
            this.sendedText.ReadOnly = true;
            this.sendedText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.sendedText.Size = new System.Drawing.Size(574, 556);
            this.sendedText.TabIndex = 6;
            this.sendedText.Text = "";
            this.sendedText.TextChanged += new System.EventHandler(this.sendedText_TextChanged);
            // 
            // portSettings
            // 
            this.portSettings.Controls.Add(this.groupBox5);
            this.portSettings.Controls.Add(this.groupBox4);
            this.portSettings.Controls.Add(this.groupBox3);
            this.portSettings.Controls.Add(this.groupBox2);
            this.portSettings.Controls.Add(this.groupBox1);
            this.portSettings.Controls.Add(this.connectButton);
            this.portSettings.Controls.Add(this.scanPortsButton);
            this.portSettings.Location = new System.Drawing.Point(12, 12);
            this.portSettings.Name = "portSettings";
            this.portSettings.Size = new System.Drawing.Size(602, 93);
            this.portSettings.TabIndex = 5;
            this.portSettings.TabStop = false;
            this.portSettings.Text = "Port Settings";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.stopBitsSelect);
            this.groupBox5.Location = new System.Drawing.Point(491, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(101, 51);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Stop bits";
            // 
            // stopBitsSelect
            // 
            this.stopBitsSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.stopBitsSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.stopBitsSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stopBitsSelect.FormattingEnabled = true;
            this.stopBitsSelect.Items.AddRange(new object[] {
            "1",
            "1.5",
            "2"});
            this.stopBitsSelect.Location = new System.Drawing.Point(7, 19);
            this.stopBitsSelect.Name = "stopBitsSelect";
            this.stopBitsSelect.Size = new System.Drawing.Size(88, 21);
            this.stopBitsSelect.TabIndex = 13;
            this.stopBitsSelect.Text = (string)this.stopBitsSelect.Items[0];
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.paritySelect);
            this.groupBox4.Location = new System.Drawing.Point(390, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(101, 51);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Parity";
            // 
            // paritySelect
            // 
            this.paritySelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.paritySelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.paritySelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.paritySelect.FormattingEnabled = true;
            this.paritySelect.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even",
            "Mark",
            "Space"});
            this.paritySelect.Location = new System.Drawing.Point(6, 19);
            this.paritySelect.Name = "paritySelect";
            this.paritySelect.Size = new System.Drawing.Size(88, 21);
            this.paritySelect.TabIndex = 11;
            this.paritySelect.Text = (string)this.paritySelect.Items[0];
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataBitsSelect);
            this.groupBox3.Location = new System.Drawing.Point(289, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(101, 51);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data bits";
            // 
            // dataBitsSelect
            // 
            this.dataBitsSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.dataBitsSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.dataBitsSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dataBitsSelect.FormattingEnabled = true;
            this.dataBitsSelect.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.dataBitsSelect.Location = new System.Drawing.Point(6, 19);
            this.dataBitsSelect.Name = "dataBitsSelect";
            this.dataBitsSelect.Size = new System.Drawing.Size(88, 21);
            this.dataBitsSelect.TabIndex = 9;
            this.dataBitsSelect.Text = (string)this.dataBitsSelect.Items[3];
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.baudrateSelect);
            this.groupBox2.Location = new System.Drawing.Point(188, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(101, 51);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Baudrate";
            // 
            // baudrateSelect
            // 
            this.baudrateSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.baudrateSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.baudrateSelect.FormattingEnabled = true;
            this.baudrateSelect.Items.AddRange(new object[] {
            "50",
            "75",
            "110",
            "150",
            "300",
            "600",
            "1200",
            "1800",
            "2000",
            "2400",
            "3600",
            "4800",
            "7200",
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "57600",
            "115200"});
            this.baudrateSelect.Location = new System.Drawing.Point(6, 19);
            this.baudrateSelect.Name = "baudrateSelect";
            this.baudrateSelect.Size = new System.Drawing.Size(88, 21);
            this.baudrateSelect.TabIndex = 7;
            this.baudrateSelect.Text = "115200";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.portNameSelect);
            this.groupBox1.Location = new System.Drawing.Point(87, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(101, 51);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Port";
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(6, 47);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 16;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // scanPortsButton
            // 
            this.scanPortsButton.Location = new System.Drawing.Point(6, 19);
            this.scanPortsButton.Name = "scanPortsButton";
            this.scanPortsButton.Size = new System.Drawing.Size(75, 23);
            this.scanPortsButton.TabIndex = 15;
            this.scanPortsButton.Text = "Scan Ports";
            this.scanPortsButton.UseVisualStyleBackColor = true;
            this.scanPortsButton.Click += new System.EventHandler(this.scanPorts_Click);
            // 
            // receiveBox
            // 
            this.receiveBox.Controls.Add(this.clearReceivedButton);
            this.receiveBox.Controls.Add(this.receivedText);
            this.receiveBox.Location = new System.Drawing.Point(612, 111);
            this.receiveBox.Name = "receiveBox";
            this.receiveBox.Size = new System.Drawing.Size(586, 609);
            this.receiveBox.TabIndex = 7;
            this.receiveBox.TabStop = false;
            this.receiveBox.Text = "Receive";
            // 
            // clearReceivedButton
            // 
            this.clearReceivedButton.Location = new System.Drawing.Point(6, 579);
            this.clearReceivedButton.Name = "clearReceivedButton";
            this.clearReceivedButton.Size = new System.Drawing.Size(75, 23);
            this.clearReceivedButton.TabIndex = 8;
            this.clearReceivedButton.Text = "Clear";
            this.clearReceivedButton.UseVisualStyleBackColor = true;
            this.clearReceivedButton.Click += new System.EventHandler(this.clearReceivedButton_Click);
            // 
            // receivedText
            // 
            this.receivedText.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.receivedText.Cursor = System.Windows.Forms.Cursors.Hand;
            this.receivedText.DetectUrls = false;
            this.receivedText.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.receivedText.Location = new System.Drawing.Point(6, 19);
            this.receivedText.Name = "receivedText";
            this.receivedText.ReadOnly = true;
            this.receivedText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.receivedText.Size = new System.Drawing.Size(574, 556);
            this.receivedText.TabIndex = 6;
            this.receivedText.Text = "";
            this.receivedText.TextChanged += new System.EventHandler(this.receivedText_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.MustAddSymbols);
            this.groupBox6.Controls.Add(this.MustAddCRLF);
            this.groupBox6.Controls.Add(this.MustAddCR);
            this.groupBox6.Location = new System.Drawing.Point(620, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(125, 93);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Message text settings";
            // 
            // MustAddSymbols
            // 
            this.MustAddSymbols.AutoSize = true;
            this.MustAddSymbols.Checked = true;
            this.MustAddSymbols.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MustAddSymbols.Location = new System.Drawing.Point(15, 70);
            this.MustAddSymbols.Name = "MustAddSymbols";
            this.MustAddSymbols.Size = new System.Drawing.Size(85, 17);
            this.MustAddSymbols.TabIndex = 1;
            this.MustAddSymbols.Text = "Add symbols";
            this.MustAddSymbols.UseVisualStyleBackColor = true;
            this.MustAddSymbols.CheckedChanged += new System.EventHandler(this.MustAddSymbols_CheckedChanged);
            // 
            // MustAddCRLF
            // 
            this.MustAddCRLF.AutoSize = true;
            this.MustAddCRLF.Location = new System.Drawing.Point(15, 46);
            this.MustAddCRLF.Name = "MustAddCRLF";
            this.MustAddCRLF.Size = new System.Drawing.Size(66, 17);
            this.MustAddCRLF.TabIndex = 0;
            this.MustAddCRLF.Text = "Add \\r\\n";
            this.MustAddCRLF.UseVisualStyleBackColor = true;
            // 
            // MustAddCR
            // 
            this.MustAddCR.AutoSize = true;
            this.MustAddCR.Checked = true;
            this.MustAddCR.Location = new System.Drawing.Point(15, 22);
            this.MustAddCR.Name = "MustAddCR";
            this.MustAddCR.Size = new System.Drawing.Size(55, 17);
            this.MustAddCR.TabIndex = 0;
            this.MustAddCR.TabStop = true;
            this.MustAddCR.Text = "Add \\r";
            this.MustAddCR.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.UseHexadecimalMode);
            this.groupBox7.Controls.Add(this.UseDecimalMode);
            this.groupBox7.Controls.Add(this.UseTextMode);
            this.groupBox7.Location = new System.Drawing.Point(751, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(108, 93);
            this.groupBox7.TabIndex = 9;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Receive mode";
            // 
            // UseHexadecimalMode
            // 
            this.UseHexadecimalMode.AutoSize = true;
            this.UseHexadecimalMode.Location = new System.Drawing.Point(15, 69);
            this.UseHexadecimalMode.Name = "UseHexadecimalMode";
            this.UseHexadecimalMode.Size = new System.Drawing.Size(86, 17);
            this.UseHexadecimalMode.TabIndex = 1;
            this.UseHexadecimalMode.Text = "Hexadecimal";
            this.UseHexadecimalMode.UseVisualStyleBackColor = true;
            // 
            // UseDecimalMode
            // 
            this.UseDecimalMode.AutoSize = true;
            this.UseDecimalMode.Location = new System.Drawing.Point(15, 46);
            this.UseDecimalMode.Name = "UseDecimalMode";
            this.UseDecimalMode.Size = new System.Drawing.Size(63, 17);
            this.UseDecimalMode.TabIndex = 0;
            this.UseDecimalMode.Text = "Decimal";
            this.UseDecimalMode.UseVisualStyleBackColor = true;
            // 
            // UseTextMode
            // 
            this.UseTextMode.AutoSize = true;
            this.UseTextMode.Checked = true;
            this.UseTextMode.Location = new System.Drawing.Point(15, 22);
            this.UseTextMode.Name = "UseTextMode";
            this.UseTextMode.Size = new System.Drawing.Size(46, 17);
            this.UseTextMode.TabIndex = 0;
            this.UseTextMode.TabStop = true;
            this.UseTextMode.Text = "Text";
            this.UseTextMode.UseVisualStyleBackColor = true;
            // 
            // TerminalSuperNova
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1210, 741);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.receiveBox);
            this.Controls.Add(this.portSettings);
            this.Controls.Add(this.sendBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TerminalSuperNova";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Terminal SuperNova";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TerminalSuperNova_FormClosing);
            this.sendBox.ResumeLayout(false);
            this.sendBox.PerformLayout();
            this.portSettings.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.receiveBox.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.ComboBox portNameSelect;
        private System.Windows.Forms.TextBox sendText;
        private System.Windows.Forms.GroupBox sendBox;
        private System.Windows.Forms.GroupBox portSettings;
        private System.Windows.Forms.RichTextBox sendedText;
        private System.Windows.Forms.ComboBox baudrateSelect;
        private System.Windows.Forms.GroupBox receiveBox;
        private System.Windows.Forms.RichTextBox receivedText;
        private System.Windows.Forms.ComboBox dataBitsSelect;
        private System.Windows.Forms.ComboBox stopBitsSelect;
        private System.Windows.Forms.ComboBox paritySelect;
        private System.Windows.Forms.Button scanPortsButton;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox MustAddSymbols;
        private System.Windows.Forms.RadioButton MustAddCRLF;
        private System.Windows.Forms.RadioButton MustAddCR;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton UseHexadecimalMode;
        private System.Windows.Forms.RadioButton UseDecimalMode;
        private System.Windows.Forms.RadioButton UseTextMode;
        private System.Windows.Forms.Button clearSendedButton;
        private System.Windows.Forms.Button clearReceivedButton;
    }
}

