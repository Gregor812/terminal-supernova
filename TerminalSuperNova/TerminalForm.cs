﻿using System;
using System.Windows.Forms;
using TerminalSuperNova.User;
using System.IO.Ports;
using System.Text;

namespace TerminalSuperNova
{
    public partial class TerminalSuperNova : Form
    {
        public TerminalSuperNova()
        {
            InitializeComponent();
            Serial serialPortWrap = new Serial();
            Serial.SerialPortHasErrors += DisplayPortErrors;
            Serial.ReadFromPort += DisplayReceivedData;
            Serial.ChangePortStatus += connectButtonTextRefresh;
            if (Serial.AvailablePorts.Length != 0)
            {
                setPortNames();
                dataBitsSelect.Text = (string)dataBitsSelect.Items[3];
                paritySelect.Text = (string)paritySelect.Items[0];
                stopBitsSelect.Text = (string)stopBitsSelect.Items[0];
            }
        }

        private void DisplayPortErrors(string msg)
        {
            receivedText.Invoke(new Action<string>((str) => receivedText.Text += str + Environment.NewLine), msg);
        }

        private void DisplayReceivedData(byte[] rawData)
        {
            string data;

            if (UseTextMode.Checked == true)
            {
                data = Encoding.ASCII.GetString(rawData);
                data = data.Replace("\0", "");
            }
            else
            {
                StringBuilder byteToText = new StringBuilder();

                if (UseDecimalMode.Checked == true)
                {
                    foreach (byte b in rawData)
                    {
                        if (b == 0x00)
                            break;
                        byteToText.Append(b.ToString("D2") + " ");
                    }
                }
                else if (UseHexadecimalMode.Checked == true)
                {
                    foreach (byte b in rawData)
                    {
                        if (b == 0x00)
                            break;
                        byteToText.Append("0x" + b.ToString("X2") + " ");
                    }
                }
                data = byteToText.ToString() + Environment.NewLine;
            }

            receivedText.Invoke(new Action<string>((str) => receivedText.Text += str), data);
        }

        private void connectButtonTextRefresh(string newText)
        {
            connectButton.Invoke(new Action<string>((str) => connectButton.Text = str), newText);
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            KeyEventArgs keyArg;
            try
            {
                keyArg = (KeyEventArgs)e;
            }
            catch
            {
                keyArg = null;
            }

            if ((sender is Button) || (keyArg != null) && (keyArg.KeyCode == Keys.Enter))
            {
                if (Serial.HasConnection)
                    SendToPort();
                else
                    receivedText.Text += (portNameSelect.Text + " is closed" + Environment.NewLine);
            }
        }

        private void SendToPort()
        {
            string sended = sendText.Text;

            sended = TransformText(sended);

            Serial.WriteToPort(sended);

            sendedText.Text += sended;
        }

        private string TransformText(string sended)
        {
            if (MustAddSymbols.Checked == true)
                if (MustAddCR.Checked == true)
                    sended = sended + "\r";
                else if (MustAddCRLF.Checked == true)
                    sended = sended + "\r\n";
            return sended;
        }

        private void scanPorts_Click(object sender, EventArgs e)
        {
            setPortNames();
        }

        private void setPortNames()
        {
            Serial.ScanPorts();

            string currentText = portNameSelect.Text;
            portNameSelect.Items.Clear();
            portNameSelect.Items.AddRange(Serial.AvailablePorts);

            if (Serial.AvailablePorts.Length != 0)
            {
                bool IsCurrentSelectionInList = false;
                foreach (string port in Serial.AvailablePorts)
                {
                    if (port == currentText)
                    {
                        IsCurrentSelectionInList = true;
                        portNameSelect.Text = currentText;
                        break;
                    }
                }
                if (!IsCurrentSelectionInList)
                    portNameSelect.Text = Serial.AvailablePorts[0];
            }
            else
                portNameSelect.Text = "";
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (Serial.HasConnection == false)
            {
                if (baudrateSelect.Text != "")
                {
                    string selectedPortName;
                    Parity selectedParity;
                    int selectedDataBits;
                    StopBits selectedStopBits;
                    GetPortSettings(out selectedPortName, out selectedParity, out selectedDataBits, out selectedStopBits);

                    try
                    {
                        int currentBaudrate = int.Parse(baudrateSelect.Text);

                        Serial.ConnectPort(selectedPortName, currentBaudrate, selectedParity, selectedDataBits, selectedStopBits);

                        PortSettingsEnable(false);
                    }
                    catch (Exception ex)
                    {
                        receivedText.Text += ex.Message + Environment.NewLine;
                    }
                }
                else
                {
                    receivedText.Text += "Please set correct baudrate" + Environment.NewLine;
                }
            }
            else
            {
                Serial.DisconnectPort();

                PortSettingsEnable(true);
            }
        }

        private void GetPortSettings(out string portName, out Parity currentParity, out int currentDataBits, out StopBits currentStopBits)
        {
            portName = portNameSelect.Text;
            currentParity = getParity();
            currentDataBits = int.Parse(dataBitsSelect.Text);
            currentStopBits = getStopBits();
        }

        private void PortSettingsEnable(bool enable)
        {
            portNameSelect.Enabled = enable;
            baudrateSelect.Enabled = enable;
            paritySelect.Enabled = enable;
            dataBitsSelect.Enabled = enable;
            stopBitsSelect.Enabled = enable;
        }

        private Parity getParity()
        {
            Parity parity = Parity.None;
            switch (paritySelect.Text)
            {
                case "None":
                    parity = Parity.None;
                    break;
                case "Odd":
                    parity = Parity.Odd;
                    break;
                case "Even":
                    parity = Parity.Even;
                    break;
                case "Mark":
                    parity = Parity.Mark;
                    break;
                case "Space":
                    parity = Parity.Space;
                    break;
            }

            return parity;
        }

        private StopBits getStopBits()
        {
            StopBits stopBits = StopBits.None;
            switch (stopBitsSelect.Text)
            {
                case "1":
                    stopBits = StopBits.One;
                    break;
                case "1.5":
                    stopBits = StopBits.OnePointFive;
                    break;
                case "2":
                    stopBits = StopBits.Two;
                    break;
            }

            return stopBits;
        }

        private void sendedText_TextChanged(object sender, EventArgs e)
        {
            sendedText.SelectionStart = sendedText.Text.Length;
            sendedText.ScrollToCaret();
        }

        private void receivedText_TextChanged(object sender, EventArgs e)
        {
            receivedText.SelectionStart = receivedText.Text.Length;
            receivedText.ScrollToCaret();
        }

        private void TerminalSuperNova_FormClosing(object sender, FormClosingEventArgs e)
        {
            Serial.DisconnectPort();
        }

        private void MustAddSymbols_CheckedChanged(object sender, EventArgs e)
        {
            MustAddCR.Enabled = MustAddSymbols.Checked;
            MustAddCRLF.Enabled = MustAddSymbols.Checked;
        }

        private void clearSendedButton_Click(object sender, EventArgs e)
        {
            sendedText.Text = "";
        }

        private void clearReceivedButton_Click(object sender, EventArgs e)
        {
            receivedText.Text = "";
        }
    }
}
